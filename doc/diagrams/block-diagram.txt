{
  /* Blocks */

  controls           [label = "controls\n(Qt5)"];
  tcp-control-client [label = "control client"];
  tcp-control-server [label = "control server"];
  display            [label = "display\n(VNC client)"];
  qemu [label = "virtual machine\n(Qemu with VNC)"];

  /* Inter-Group Edges */

  tcp-control-client <-> tcp-control-server [label = "JSON over TCP", fontsize = 5];
  display <- qemu;

  /* Groups */

  group {
    label = "view";
    color = "lightcyan";
    controls -> tcp-control-client;
    display
  }
  group {
    label = "controller";
    color = "thistle";
    tcp-control-server -> qemu [folded];
  }
}
