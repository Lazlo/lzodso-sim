#!/bin/bash

set -e
set -u

pkgs=""
pkgs="$pkgs make"
pkgs="$pkgs python3"
pkgs="$pkgs python3-pip"
pkgs="$pkgs python3-pyqt5"
pkgs="$pkgs qemu"
pkgs="$pkgs x11-utils"
pkgs="$pkgs tigervnc-viewer"

apt-get update && apt-get -q install -y $pkgs
pip3 install --system -r requirements.txt
