#!/bin/bash

set -u

main() {
	if [ $# != 1 ]; then
		echo "USAGE: $(basename $0) <yocto-build-dir>"
		exit 1
	fi
	local -r yp_builddir="$1"
	local -r yp_builddir_name="$(basename $yp_builddir)"
	local -r yp_basedir="$(dirname $yp_builddir)"
	pushd $yp_basedir > /dev/null
	# Source build environment file but hide the output
	# Also temporarily allow for undefined variables
	set +u
	. oe-init-build-env $yp_builddir_name > /dev/null
	set -u
	# NOTE "kvm" option is only possible when built for x86-64
	# TODO Set keyboard layout
	runqemu kvm qemuparams="-S -monitor telnet::5555,server,nowait -vnc :0" bootparams="quiet"
	popd > /dev/null # $yp_basedir
}

main $@
