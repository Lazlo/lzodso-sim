# Oscilloscope Simulator

Simulator to support software development.

![screenshot](doc/pictures/screenshot.png)

It uses Python 3, Qt 5, Qemu and a VNC client to create a user interface that
mimics the one of a physical oscilloscope.

The simulator consists of two applications. The **view** that is responsible
for the user interface. When one of the controls in the view are modified
by the user, the view forwards the event to the **controller**.

The controller is in charge of maintaining the logical state of the simulated
oscilloscope. It also takes care of starting a real machine emulator (Qemu) in
VNC video mode. This allows the display output of the emulated machine to
be forwarded to a VNC client that is part of the view.

![block diagram](doc/diagrams/block-diagram.svg)

## Build Instructions

```
git clone git@gitlab.com:Lazlo/lzodso-sim.git
cd lzodso-sim
sudo ./setup.sh
```

 1. clone the simulator repository and change into the directory
 2. run ```setup.sh``` as root to install dependencies

### Dependencies

Running the ```setup.sh``` script will take care of installing the dependencies
listed below.

 * python3
 * python3-pyqt5
 * x11-utils
 * qemu
 * tigervnc-viewer

## Usage

Start the controller server.

```
./lzodso/controller/controller.py
```

Start the view application.

```
./lzodso/view/view.py
```

### Use With Yocto Project

In order to have the screen controlled by a Qemu that has booted a image
built using the Yocto Project, the following steps need to be taken.

 1. create a image using Yocto (see below for an example)
 2. in one terminal run ```yp-runqemu.sh <path-to-yocto-build-directory>``` from this repo
 3. in another terminal start the controller application with option ```-y```
 4. in yet another terminal start the view application

```
./yp-runqemu ~/poky/build-qemux86-64
./lzodso/controller/controller.py -y
./lzodso/view/view.py
```

The following lines give an example how to build and image using the Yocto Project.

```
git clone -b zeus git://git.yoctoproject.org/poky
cd poky
source oe-init-build-env build-qemux86-64
echo 'MACHINE=qemux86-64' >> conf/local.conf
bitbake core-image-sato-dev
```

## Development

### Run Unit Tests

```
make
```

## To Do

### Bugs

 * qemu is not terminated when simulator is closed

### Features

#### Controller

 * pyqemu: implement monitor via QMP/telnet
 * do not start when Qemu terminates after launching with non-zero exitcode

#### View

 * have server address and port as command line options
 * have VNC client reconnect when VNC server closes connection
 * integrate python-mini-inport
