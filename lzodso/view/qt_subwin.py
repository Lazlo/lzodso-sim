# Depends on xwininfo from x11-utils Debian package
def get_winid_by_name(win_name):
	import subprocess
	import re
	import time
	timeout_sec = 2.0
	sleep_sec = 0.1
	while timeout_sec > 0.0:
		p = subprocess.Popen(["xwininfo", "-name", win_name], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
		out, err = p.communicate()
		m = re.search("Window id: (?P<win_id>0x[0-9a-f]+)", out.decode("utf-8"))
		if m:
			win_id = int(m.group("win_id"), 16)
			return win_id
		timeout_sec = timeout_sec - sleep_sec
		time.sleep(sleep_sec)

def create_widget_from_winid(win_id):
	from PyQt5.QtCore import Qt
	from PyQt5.QtGui import QWindow
	from PyQt5.QtWidgets import QWidget
	win = QWindow.fromWinId(win_id)
	win.setFlags(Qt.FramelessWindowHint)
	return QWidget.createWindowContainer(win)

def create_widget_from_winname(win_name):
	win_id = get_winid_by_name(win_name)
	return create_widget_from_winid(win_id)

def launch_process(cmd):
	import subprocess
	return subprocess.Popen(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

# Depends on qemu (compiled with sdl support enabled)
def create_qemu_widget():
	qemu_bin = "/home/lazlo/src/qemu/x86_64-softmmu/qemu-system-x86_64"
	qemu_args = "-display sdl"
	import pyqemu
	q = pyqemu.Launcher()
	q.set_cmd_bin(qemu_bin)
	q.add_cmd_args(qemu_args.split(" "))
	q.launch()
	qemu_win_name = "QEMU [Stopped]"
	return create_widget_from_winname(qemu_win_name)

def create_vnc1_widget():
	vnc_bin = "xtightvncviewer"
	vnc_args = "localhost"
	cmd = [vnc_bin, vnc_args]
	launch_process(cmd)
	vnc_win_name = "TightVNC: QEMU"
	return create_widget_from_winname(vnc_win_name)

def create_vnc2_widget():
	vnc_bin = "xtigervncviewer"
	vnc_args = "localhost::5900"
	cmd = [vnc_bin, "-shared", vnc_args]
	launch_process(cmd)
	vnc_win_name = "QEMU - TigerVNC"
	return create_widget_from_winname(vnc_win_name)
