#!/usr/bin/python3

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QDial, QPushButton, QHBoxLayout, QVBoxLayout, QGroupBox

class DisplaySection(QWidget):

	def __init__(self, n_gpbuttons, eh):
		super().__init__()
		self.initUI(n_gpbuttons, eh)

	def initUI(self, n_gpbuttons, eh):
		# vertical layout to keep display and general purpose buttons in
		self._layout = QVBoxLayout()
		self.setLayout(self._layout)
		layout_gpbtn = QHBoxLayout()
		# create widgets
		import qt_subwin
		self._display = qt_subwin.create_vnc2_widget()
		"""
		self._display = QLabel("display not connected")
		"""
		self._buttons = []
		for i in range(n_gpbuttons):
			caption = "F%i" % (i + 1)
			objectName = "btn_gp%i" % (i + 1)
			b = QPushButton(caption, objectName=objectName)
			self._buttons.append(b)
		# tweak widgets
		"""
		self._display.setAlignment(Qt.AlignCenter)
		self._display.resize(800, 600)
		self._display.setAutoFillBackground(True)
		p = self._display.palette()
		p.setColor(self._display.backgroundRole(), Qt.gray)
		self._display.setPalette(p)
		"""
		# connect handlers
		for b in self._buttons:
			b.clicked.connect(eh)
		# add to layout
		self._layout.addWidget(self._display)
		self._layout.addLayout(layout_gpbtn)
		for b in self._buttons:
			layout_gpbtn.addWidget(b)

class MenuButtonSection(QGroupBox):

	def __init__(self, eh_set):
		super().__init__("")
		self.initUI(eh_set)

	def initUI(self, eh_set):
		self._layout = QVBoxLayout()
		self.setLayout(self._layout)
		self._layout_top = QHBoxLayout()
		self._layout_bottom = QHBoxLayout()
		self._layout.addLayout(self._layout_top)
		self._layout.addLayout(self._layout_bottom)
		# create widgets
		self._cursor = QPushButton("Cursor", objectName="btn_cursor")
		self._acquire = QPushButton("Acquire", objectName="btn_acquire")
		self._saverecall = QPushButton("Save/Recall", objectName="btn_saverecall")
		self._measure = QPushButton("Measure", objectName="btn_measure")
		self._display = QPushButton("Display", objectName="btn_display")
		self._util = QPushButton("Util", objectName="btn_util")
		# connect handlers
		self._cursor.clicked.connect(eh_set['btn_cursor'])
		self._acquire.clicked.connect(eh_set['btn_acquire'])
		self._saverecall.clicked.connect(eh_set['btn_saverecall'])
		self._measure.clicked.connect(eh_set['btn_measure'])
		self._display.clicked.connect(eh_set['btn_display'])
		self._util.clicked.connect(eh_set['btn_util'])
		# add to layout
		self._layout_top.addWidget(self._cursor)
		self._layout_top.addWidget(self._acquire)
		self._layout_top.addWidget(self._saverecall)
		self._layout_bottom.addWidget(self._measure)
		self._layout_bottom.addWidget(self._display)
		self._layout_bottom.addWidget(self._util)

class RunControls(QGroupBox):

	def __init__(self, eh_set):
		super().__init__("")
		self.initUI(eh_set)

	def initUI(self, eh_set):
		self._layout = QVBoxLayout()
		self.setLayout(self._layout)
		# create widdgets
		self._runstop = QPushButton("Run/Stop", objectName="btn_runstop")
		self._autosetup = QPushButton("Auto Setup", objectName="btn_autosetup")
		self._default = QPushButton("Default", objectName="btn_default")
		# connect handlers
		self._runstop.clicked.connect(eh_set['btn_runstop'])
		self._autosetup.clicked.connect(eh_set['btn_autosetup'])
		self._default.clicked.connect(eh_set['btn_default'])
		# add to layout
		self._layout.addWidget(self._runstop)
		self._layout.addWidget(self._autosetup)
		self._layout.addWidget(self._default)

class ChannelControls(QGroupBox):

	def __init__(self, channel_no, eh_set):
		super().__init__('CH %d' % channel_no)
		self.channel_no = channel_no
		self.initUI(eh_set)

	def initUI(self, eh_set):
		self.setAlignment(Qt.AlignCenter)
		self._layout = QVBoxLayout()
		self.setLayout(self._layout)
		# create widgets
		self._onoff = QPushButton('On/Off', objectName='btn_onoff')
		self._vdiv_label = QLabel('V/div')
		self._vdiv = QDial(objectName='dial_vdiv')
		self._math = QPushButton('Math', objectName='btn_math')
		self._ref = QPushButton('Ref', objectName='btn_ref')
		self._offset_label = QLabel('Offset')
		self._offset = QDial(objectName='dial_offset')
		# tune widgets
		self._onoff.setCheckable(True)
		self._vdiv_label.setAlignment(Qt.AlignCenter|Qt.AlignBottom)
		self._vdiv.setWrapping(True)
		self._offset_label.setAlignment(Qt.AlignCenter|Qt.AlignBottom)
		self._offset.setWrapping(True)
		# connect handlers
		self._onoff.clicked.connect(eh_set['btn_onoff'])
		self._vdiv.valueChanged.connect(eh_set['dial_vdiv'])
		self._math.clicked.connect(eh_set['btn_math'])
		self._ref.clicked.connect(eh_set['btn_ref'])
		self._offset.valueChanged.connect(eh_set['dial_offset'])
		# add widgets to layout
		self._layout.addWidget(self._onoff)
		self._layout.addWidget(self._vdiv_label)
		self._layout.addWidget(self._vdiv)
		self._layout.addWidget(self._math)
		self._layout.addWidget(self._ref)
		self._layout.addWidget(self._offset_label)
		self._layout.addWidget(self._offset)

class ChannelSection(QGroupBox):

	def __init__(self, eh_set, n_channels):
		super().__init__("Vertical")
		self.initUI(eh_set, n_channels)

	def initUI(self, eh_set, n_channels):
		self.setAlignment(Qt.AlignCenter)
		self._layout = QHBoxLayout()
		self.setLayout(self._layout)
		# create widgets
		self._channels = []
		for i in range(n_channels):
			ch_num = i + 1
			ch = ChannelControls(ch_num, eh_set[0])
			self._channels.append(ch)
		# add to layout
		for ch in self._channels:
			self._layout.addWidget(ch)

class HorizontalControls(QGroupBox):

	def __init__(self, eh_set):
		super().__init__("Horizontal")
		self.initUI(eh_set)

	def initUI(self, eh_set):
		self.setAlignment(Qt.AlignCenter)
		self._layout = QVBoxLayout()
		self.setLayout(self._layout)
		# create widgets
		self._sdiv_label = QLabel("S/div")
		self._sdiv = QDial(objectName='dial_sdiv')
		self._pos_label = QLabel("Position")
		self._pos = QDial(objectName='dial_pos')
		# tune widgets
		self._sdiv_label.setAlignment(Qt.AlignCenter|Qt.AlignBottom)
		self._pos_label.setAlignment(Qt.AlignCenter|Qt.AlignBottom)
		self._sdiv.setWrapping(True)
		self._pos.setWrapping(True)
		# connect handlers
		self._sdiv.valueChanged.connect(eh_set['dial_sdiv'])
		self._pos.valueChanged.connect(eh_set['dial_pos'])
		# add widgets to layout
		self._layout.addWidget(self._sdiv_label)
		self._layout.addWidget(self._sdiv)
		self._layout.addWidget(self._pos_label)
		self._layout.addWidget(self._pos)

class TriggerControls(QGroupBox):

	def __init__(self, eh_set):
		super().__init__("Trigger")
		self.initUI(eh_set)

	def initUI(self, eh_set):
		self.setAlignment(Qt.AlignCenter)
		self._layout = QVBoxLayout()
		self.setLayout(self._layout)
		# create widgets
		self._setup = QPushButton('Setup', objectName="btn_setup")
		self._auto = QPushButton('Auto', objectName="btn_auto")
		self._normal = QPushButton('Normal', objectName="btn_normal")
		self._single = QPushButton('Single', objectName="btn_single")
		self._level_label = QLabel('Level')
		self._level = QDial(objectName="dial_level")
		# tune widgets
		self._level_label.setAlignment(Qt.AlignCenter|Qt.AlignBottom)
		self._level.setWrapping(True)
		# Connect handlers
		self._setup.clicked.connect(eh_set['btn_setup'])
		self._auto.clicked.connect(eh_set['btn_auto'])
		self._normal.clicked.connect(eh_set['btn_normal'])
		self._single.clicked.connect(eh_set['btn_single'])
		self._level.valueChanged.connect(eh_set['dial_level'])
		# add widgets to layout
		self._layout.addWidget(self._setup)
		self._layout.addWidget(self._auto)
		self._layout.addWidget(self._normal)
		self._layout.addWidget(self._single)
		self._layout.addWidget(self._level_label)
		self._layout.addWidget(self._level)

from PyQt5.QtWidgets import QScrollArea, QLineEdit

class Internals(QWidget):

	def __init__(self):
		super().__init__()
		self.initUI()

	def initUI(self):
		self._layout = QVBoxLayout()
		self.setLayout(self._layout)
		cg_layout = QHBoxLayout()
		# create widgets
		self._conn_group = QGroupBox("Connections")
		self._conn_group.setLayout(cg_layout)
		self._ctrlhost_label = QLabel("Controller Host")
		self._ctrlhost_value = QLineEdit()
		self._ctrlport_label = QLabel("Controller Port")
		self._ctrlport_value = QLineEdit()
		self._console = QLabel(".")
		self._scrollbox = QScrollArea()
		# tweak widgets
		self._ctrlhost_value.setText("localhost")
		self._ctrlport_value.setText("6000")
		self._console.setAlignment(Qt.AlignTop)
		self._console.setAutoFillBackground(True)
		p = self._console.palette()
		p.setColor(self._console.backgroundRole(), Qt.gray)
		self._console.setPalette(p)
		self._scrollbox.setWidget(self._console)
		self._scrollbox.setWidgetResizable(True)
#		self._scrollbox.setFixedHeight(400)
		self._scrollbox.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
		# add to layout
		cg_layout.addWidget(self._ctrlhost_label)
		cg_layout.addWidget(self._ctrlhost_value)
		cg_layout.addWidget(self._ctrlport_label)
		cg_layout.addWidget(self._ctrlport_value)
		self._layout.addWidget(self._conn_group)
		self._layout.addWidget(self._scrollbox)

	# FIXME When automatically setting the scrollbar
	# to maximum, we are always one entry off from the actual bottom.
	# Hence we can not see the most recent line!
	def appendToConsole(self, text):
		# append text to console
		newtext = "%s\n%s" % (self._console.text(), text)
		self._console.setText(newtext)
		# scroll down
		vbar = self._scrollbox.verticalScrollBar()
		vbar.setValue(vbar.maximum())

class Request(object):

	def __init__(self, kind):
		self.kind = kind

class ControllerClient():

	def __init__(self, host, port):
		self.server_host = host
		self.server_port = port
		self.sock =  None

	def open(self):
		import socket
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.connect((self.server_host, self.server_port))

	def close(self):
		self.sock.close()

	def send_msg(self, msg):
		self.sock.sendall(bytes(msg, "utf-8"))
		resp = self.sock.recv(512)
		return resp

	def send_json(self, obj):
		import json
		msg = json.dumps(obj.__dict__)
		logging.debug("send_json(%s)" % msg)
		resp = self.send_msg(msg)
		return json.loads(resp)

	def get_hw_desc(self):
		return self.send_json(Request("GetHardwareDesc"))

class KeyChangeEvent(Request):

	def __init__(self, key_id, key_val):
		super().__init__(self.__class__.__name__)
		self.key_id = key_id
		self.key_val = key_val

import logging

logging.basicConfig(level=logging.DEBUG)

class Oscilloscope(QWidget):

	def __init__(self, server_host="localhost", server_port=6000):
		super().__init__()
		self.client = ControllerClient(server_host, server_port)
		self.client.open()
		self.hw_desc = self.client.get_hw_desc()
		self.handlers = self.createEventHandlersDict()
		self.initUI(self.hw_desc['n_channels'], self.hw_desc['n_gpbuttons'], self.handlers)

	def __del__(self):
		self.client.close()

	def createEventHandlersDict(self):
		handlers = {
			'btn_pwr_pressed':		self.handle_btn_pwr_pressed,
			'btn_pwr_released':		self.handle_btn_pwr_released,
			'btn_gp':			self.handle_common,
			'ch': [
				{
					'btn_onoff':	self.handle_common,
					'btn_ref':	self.handle_common,
					'btn_math':	self.handle_common,
					'dial_vdiv':	self.handle_common,
					'dial_offset':	self.handle_common,
				},
			],
			'acqu': {
				'dial_sdiv':		self.handle_common,
				'dial_pos':		self.handle_common,
			},
			'trig': {
				'btn_setup':		self.handle_common,
				'btn_auto':		self.handle_common,
				'btn_normal':		self.handle_common,
				'btn_single':		self.handle_common,
				'dial_level':		self.handle_common,
			},
			'dial_adjust':			self.handle_common,
			'menu': {
				'btn_cursor':		self.handle_common,
				'btn_acquire':		self.handle_common,
				'btn_saverecall':	self.handle_common,
				'btn_measure':		self.handle_common,
				'btn_display':		self.handle_common,
				'btn_util':		self.handle_common,
			},
			'runctl': {
				'btn_runstop':		self.handle_common,
				'btn_autosetup':	self.handle_common,
				'btn_default':		self.handle_common,
			}
		}
		return handlers

	def initUI(self, n_channels, n_gpbuttons, eh_dict):
		self.setWindowTitle("lzoDSO Simulator")
		self._layout = QVBoxLayout()
		self.setLayout(self._layout)
		device_hboxlayout = QHBoxLayout()
		self._layout.addLayout(device_hboxlayout)
		# create widgets
		self._display = DisplaySection(n_gpbuttons, eh_dict['btn_gp'])
		self._menu = MenuButtonSection(eh_dict['menu'])
		self._run = RunControls(eh_dict['runctl'])
		self._channels = ChannelSection(eh_dict['ch'], n_channels)
		self._horizontal = HorizontalControls(eh_dict['acqu'])
		self._trigger = TriggerControls(eh_dict['trig'])
		self._internals = Internals()
		# add to layouts
		# Left side
		device_hboxlayout.addWidget(self._display)
		# Right side
		# on the right side of the screen, we need to insert a VBoxLayout
		# to allow us to have the various menu and run control buttons
		# above the channel, sampling and trigger controls.
		layout_right = QVBoxLayout()
		layout_right_top = QHBoxLayout()
		layout_right_bottom = QHBoxLayout()
		layout_right.addLayout(layout_right_top)
		layout_right.addLayout(layout_right_bottom)
		device_hboxlayout.addLayout(layout_right)
		# Right Top
		self.add_right_top_gpdial(layout_right_top, eh_dict['dial_adjust'])
		layout_right_top.addWidget(self._menu)
		layout_right_top.addWidget(self._run)
		# right bottom
		layout_right_bottom.addWidget(self._channels)
		layout_right_bottom.addWidget(self._horizontal)
		layout_right_bottom.addWidget(self._trigger)
		# Bottom row, where the power button and connectors go.
		b = QPushButton("Power")
		b.pressed.connect(eh_dict['btn_pwr_pressed'])
		b.released.connect(eh_dict['btn_pwr_released'])
		self._layout.addWidget(b)
		# internals section below device
		self._layout.addWidget(self._internals)

	def add_right_top_gpdial(self, parent_hboxlayout, eh):
		# Create the control elements
		ctl_dial_adjust = QDial(objectName='dial_adjust')
		ctl_dial_adjust.setWrapping(True)
		ctl_dial_adjust.valueChanged.connect(eh)
		# Perform layouting
		layout_rt1 = QVBoxLayout()
		l = QLabel("Adjust")
		l.setAlignment(Qt.AlignCenter)
		#l.setAlignment(QTestStream.AlignCenter)
		layout_rt1.addWidget(l)
		layout_rt1.addWidget(ctl_dial_adjust)
		parent_hboxlayout.addLayout(layout_rt1)

	#
	# Handlers and helpers to them
	#

	def obj_to_controller(self, obj):
		self.client.send_json(obj)
		self._internals.appendToConsole(obj.__dict__)

	def handle_common(self, val):
		sender = self.sender()
		objectName = sender.objectName()
		self.obj_to_controller(KeyChangeEvent(objectName, val))

	# Power Button

	def handle_btn_pwr(self, val):
		self.obj_to_controller(KeyChangeEvent("btn_pwr", val))

	def handle_btn_pwr_pressed(self):
		self.handle_btn_pwr("pressed")

	def handle_btn_pwr_released(self):
		self.handle_btn_pwr("released")

if __name__ == "__main__":
	app = QApplication([])
	window = Oscilloscope()
	window.show()
	app.exec_()
