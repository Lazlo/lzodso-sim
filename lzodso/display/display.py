#!/usr/bin/python3

import pygame

class Display():

	def __init__(self):
		return

def draw_grid(surface, width, height, color):
	divisions_vertical = 10
	divisions_horizontal = 10

	height_per_div = height / divisions_vertical
	width_per_div = width / divisions_horizontal

	for i in range(1, divisions_vertical):
		pos = (0, i * height_per_div)
		r1 = pygame.Rect(pos, (width, 1))
		pygame.draw.rect(surface, color, r1)

	for i in range(1, divisions_horizontal):
		pos = (i * width_per_div, 0)
		r2 = pygame.Rect(pos, (1, height))
		pygame.draw.rect(surface, color, r2)

def main(width, height):
	color_black = (0, 0, 0)
	color_green = (0, 255, 0)

	pygame.init()
	screen = pygame.display.set_mode((width, height))

	surface = pygame.Surface(screen.get_size())
	surface = surface.convert()
	surface.fill(color_black)

	draw_grid(surface, width, height, color_green)

	screen.blit(surface, (0, 0))

	while True:
		pygame.display.flip()
		pygame.display.update()
		import time
		time.sleep(0.5)

if __name__ == "__main__":
	main(800, 600)
