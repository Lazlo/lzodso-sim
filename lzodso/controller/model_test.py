import unittest
import model

class ModelTestCase(unittest.TestCase):

	def setUp(self):
		self.expected_channels = 4
		self.expected_gpbuttons = 10
		self.m = model.Model(self.expected_channels, self.expected_gpbuttons)

	def tearDown(self):
		return

	def test_init_sets_channels(self):
		# TODO Query using method. To not poke around inside of object!
		expected = self.expected_channels
		actual = self.m.channels
		self.assertEqual(actual, expected)

	def test_init_sets_gpbuttons(self):
		expected = self.expected_gpbuttons
		actual = self.m.gpbuttons
		self.assertEqual(actual, expected)

	def test_get_hw_desc(self):
		expected = {}
		expected.update({'n_channels': self.expected_channels})
		expected.update({'n_gpbuttons': self.expected_gpbuttons})
		actual = self.m.get_hw_desc()
		self.assertEqual(actual, expected)

	def test_reset_sets_powered_to_false(self):
		expected = False
		self.assertEqual(self.m.powered, expected)

	def test_reset_sets_all_channels_to_on(self):
		expected = [True] * self.expected_channels
		self.assertEqual(self.m.channel_on_state, expected)

	def test_reset_sets_all_channels_vdiv_to_1v(self):
		expected = [1] * self.expected_channels
		self.assertEqual(self.m.channel_vdiv_state_volt, expected)

	def test_reset_sets_all_channels_offset_volt_to_0v(self):
		expected = [0] * self.expected_channels
		self.assertEqual(self.m.channel_offset_volt, expected)

	def test_reset_sets_acqu_time_per_div_to_1s(self):
		self.assertEqual(self.m.acqu_time_per_div_sec, 1)

	def test_reset_sets_acqu_display_offset_to_0percent(self):
		self.assertEqual(self.m.acqu_display_offset_percent, 0)

	def test_reset_sets_trigger_source_to_fist_channel(self):
		self.assertEqual(self.m.trigger_source, 1)

	def test_reset_sets_trigger_mode_to_rising_edge(self):
		TRIGGER_MODE_RISING_EDGE = 1
		self.assertEqual(self.m.trigger_mode, TRIGGER_MODE_RISING_EDGE)

	def test_reset_sets_trigger_level_to_1v(self):
		self.assertEqual(self.m.trigger_level_volt, 1)

	def test_reset_sets_trigger_holdof_to_0s(self):
		self.assertEqual(self.m.trigger_holdoff_sec, 0)
