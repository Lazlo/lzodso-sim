import unittest
from daemon import Daemon

class DaemonInitTestCase(unittest.TestCase):

	def test_daemon_model_default_is_none(self):
		d = Daemon()
		self.assertEqual(d.model, None)

	def test_daemon_model_set_from_keyword_args(self):
		expected = "not-a-real-model-class"
		d = Daemon(model=expected)
		self.assertEqual(d.model, expected)

	def test_daemon_port_default_is_none(self):
		d = Daemon()
		self.assertEqual(d.port, None)

	def test_daemon_port_is_set_from_keyword_args(self):
		expected = 6000
		d = Daemon(port=expected)
		self.assertEqual(d.port, expected)
