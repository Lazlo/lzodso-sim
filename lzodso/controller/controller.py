#!/usr/bin/python3

import logging

class JsonRpcServer():

	def __init__(self, listen_addr, listen_port):
		self.listen_addr = listen_addr
		self.listen_port = listen_port
		self.lsock = None

	def open(self):
		import socket
		self.lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.lsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.lsock.bind((self.listen_addr, self.listen_port))
		self.lsock.listen(1)

	def handle_connection(self, handle_request):
		csock, addr = self.lsock.accept()
		logging.debug("connected")
		while True:
			data = csock.recv(128)
			if not data:
				break
			resp = handle_request(data)
			csock.sendall(bytes(resp, "utf-8"))
		logging.debug("closing connection")
		csock.close()

class LegacyDaemon():

	def __init__(self, model, opt_yp_runqemu=False, listen_addr="localhost", listen_port=6000):
		self.jrs = JsonRpcServer(listen_addr, listen_port)
		self.model = model
		self.opt_yp_runqemu = opt_yp_runqemu
		self.q = None

	def handle_request(self, data):
		import json
		resp = json.dumps({'kind': 'error'})
		try:
			req = json.loads(data)
		except Exception as e:
			logging.error("%s when decoding \"%s\"" % (e, data))
			return resp
		print(req)
		# Dispatch among different kind of requests
		if req["kind"] == "KeyChangeEvent":
			if req["key_id"] == "btn_pwr":
				if req["key_val"] == "released":
					self.model.powered = not self.model.powered
					logging.debug("set model.powered to %s" % self.model.powered)
					if self.model.powered:
						logging.debug("Sending 'continue' to Qemu monitor")
						if not self.opt_yp_runqemu:
							self.q.cont()
						else:
							self.q.write(b'cont\r\n')
					else:
						logging.debug("FIXME call pyqemu.system_powerdown")
			# For now we confirm all KeyChangeEvent requests
			# even though we do not check validity or
			# dispatch any action for them.
			resp = json.dumps({'kind': 'confirmation'})
		if req["kind"] == "GetHardwareDesc":
			hw_desc = self.model.get_hw_desc()
			resp = json.dumps(hw_desc)
		return resp

	def run(self):
		if not self.opt_yp_runqemu:
			import qemu
			self.q = qemu.Launcher()
			self.q.add_cmd_args(["-vnc", ":0"])
			logging.debug("qemu_cmd = %s" % self.q.get_cmd())
			logging.debug("Starting Qemu ...")
			self.q.launch()
		else:
			from qemu import TelnetMonitor
			self.q = TelnetMonitor()
			logging.debug("Connecting to Qemu Monitor using Telnet ...")
			self.q.open()
		logging.debug("Starting JSON RPC service ...")
		self.jrs.open()
		while True:
			logging.debug("listenting ...")
			self.jrs.handle_connection(self.handle_request)
			"""
			# After client disconnects, show the model state
			d = self.model.__dict__
			import json
			print(json.dumps(d, indent=8, sort_keys=True))
			"""

if __name__ == "__main__":
	logging.basicConfig(level=logging.DEBUG)
	import optparse
	parser = optparse.OptionParser()
	parser.add_option("-y",
				action="store_true", dest="yp_runqemu", default=False,
				help="Do not start qemu - user needs to execute 'runqemu' from Yocto Project build directory.")
	(options, args) = parser.parse_args()
	logging.debug("options.yp_runqemu = %s" % options.yp_runqemu)
	n_channels = 4
	n_gpbuttons = 7
	from model import Model
	m = Model(n_channels, n_gpbuttons)
	d = LegacyDaemon(m, options.yp_runqemu)
	d.run()
