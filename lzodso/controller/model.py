class Model():

	TRIGGER_MODE_RISING_EDGE = 1

	def __init__(self, channels, gpbuttons):
		self.channels = channels
		self.gpbuttons = gpbuttons
		self.reset()

	def reset(self):
		self.powered = False
		self.channel_on_state = [True] * self.channels
		self.channel_vdiv_state_volt = [1] * self.channels
		self.channel_offset_volt = [0] * self.channels
		self.acqu_time_per_div_sec = 1
		self.acqu_display_offset_percent = 0
		self.trigger_source = 1
		self.trigger_mode = self.TRIGGER_MODE_RISING_EDGE
		self.trigger_level_volt = 1
		self.trigger_holdoff_sec = 0

	def get_hw_desc(self):
		hw_desc = {}
		hw_desc.update({'n_channels': self.channels})
		hw_desc.update({'n_gpbuttons': self.gpbuttons})
		return hw_desc
