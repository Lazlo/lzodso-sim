class Daemon():

	def __init__(self, **kwargs):
		self.__reset()
		for k in kwargs.keys():
			v = kwargs[k]
			if k == 'model':
				self.__model = v
			elif k == 'port':
				self.__port = v
	def __reset(self):
		self.__model = None
		self.__port = None

	@property
	def model(self):
		return self.__model

	@property
	def port(self):
		return self.__port
