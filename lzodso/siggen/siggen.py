class SigGen():

	WAVEFORM_SQUARE = 1

	def set_frequency(self, freq_hz):
		self.freq_hz = freq_hz

	def set_duty(self, duty_percent):
		self.duty_percent = duty_percent

	def set_step(self, step_mV):
		self.step_mV = step_mV

	def set_max(self, max_mV):
		self.max_mV = max_mV

	def set_min(self, min_mV):
		self.min_mV = min_mV

	def set_waveform(self, waveform_type):
		self.waveform_type = waveform_type
