import unittest
import siggen

class SigGenTestCase(unittest.TestCase):

	def setUp(self):
		self.sg = siggen.SigGen()

	def test_set_frequency(self):
		expected_hz = 1000
		self.sg.set_frequency(expected_hz)
		actual = self.sg.freq_hz
		self.assertEqual(expected_hz, actual)

	def test_set_duty(self):
		expected_percent = 50
		self.sg.set_duty(expected_percent)
		actual = self.sg.duty_percent
		self.assertEqual(expected_percent, actual)

	def test_set_step(self):
		expected_step_mV = 500
		self.sg.set_step(expected_step_mV)
		actual = self.sg.step_mV
		self.assertEqual(expected_step_mV, actual)

	def test_set_max(self):
		expected_max_mV = 5000
		self.sg.set_max(expected_max_mV)
		actual = self.sg.max_mV
		self.assertEqual(expected_max_mV, actual)

	def test_set_min(self):
		expected_min_mV = 0
		self.sg.set_min(expected_min_mV)
		actual = self.sg.min_mV
		self.assertEqual(expected_min_mV, actual)

	def test_set_waveform(self):
		expected_waveform_type = siggen.SigGen.WAVEFORM_SQUARE
		self.sg.set_waveform(expected_waveform_type)
		actual = self.sg.waveform_type
		self.assertEqual(expected_waveform_type, actual)

if __name__ == "__main__":
	unittest.main()
