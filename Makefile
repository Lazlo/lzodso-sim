test_cmd=python3 -m xmlrunner discover -p"*_test.py" -s lzodso/controller
pkg_cmd=python3 setup.py bdist_wheel sdist

test:
	$(test_cmd)
vtest:
	$(test_cmd) -v
doc:
	blockdiag -T svg -o doc/diagrams/block-diagram.svg doc/diagrams/block-diagram.txt
package:
	$(pkg_cmd)
clean:
	$(RM) -rf */__pycache__ */*/__pycache__ TEST-*.xml
	$(RM) -rf build dist *.egg-info

.PHONY: doc
