import setuptools

with open('README.md', 'r') as fh:
	long_description = fh.read()

setuptools.setup(
	name='lzodso',
	version='0.3.0',
	scripts=[],
	author="Daniel Laszlo Sitzer",
	author_email="dlsitzer@gmail.com",
	description="...",
	long_description=long_description,
	long_description_content_type='text/markdown',
	url='https://gitlab.com/Lazlo/lzodso-sim',
	packages=setuptools.find_packages(),
	classifiers=[
		"Programming Languate :: Python :: 3",
	],
)
